package utilita;

import java.util.ArrayList;

/**
 * Classe per l'analisi frequenziale di un testo
 * 
 * @author Michele Rizzo
 * @version 1.0
 *
 */
public class Frequenze
{

	/**
	 * Metodo che calcola la frequenza del carattere c nella stringa msg
	 * @param c carattere del quale voglio calcolare la frequenza
	 * @param msg messaggio
	 * @return frequenza del carattere desiderato nella stringa msg
	 */
	public static double calcolaFrequenza(char c, String msg)
	{
		int n = msg.length();
		int occorrenza = 0;
		
		for(int i = 0; i < n; i++)
		{
			char x = msg.charAt(i);
			
			if(x == c)
				occorrenza++;
		}
		
		return (double)occorrenza/n;
	}
	
	/**
	 * Metoto che restituisce le frequenze dei caratteri della stringa alfabeto presenti nella stringa msg
	 * @param alfabeto stringa di caratteri dei quali si vuole ottenere la frequenza
	 * @param msg
	 * @return array con le frequenze dei caratteri desiderati
	 */
	public static ArrayList<Double> calcolaFrequenze(String alfabeto, String msg)
	{
		int n = msg.length();
		int m = alfabeto.length();
		
		ArrayList<Double> frequenze = new ArrayList<Double>(m);
		
		
		for(int i = 0; i < m; i++)
		{
			char a = alfabeto.charAt(i);
			
			double frequenza = calcolaFrequenza(a, msg);
			frequenze.add(frequenza);
		
		}
		
		return frequenze;

	}
}
