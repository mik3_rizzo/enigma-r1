package main;

import java.io.File;

import utilita.Frequenze;
import utilita.Utilitas;


/**
 * Classe principale del programma.
 * 
 * @author Michele Rizzo
 * @version 1.0
 *
 */
public class Main
{
	public static final String ALFABETO = "abcdefghijklmnopqrstuvwxyz";
	public static final String CIFRATO =  "nopqhrslvwmziabcdefgkyjtux";
	
	public static final String PLAIN_PATH = "C:/Users/mrizz/Desktop/Workspaces/TESI/plaintext.txt";
	public static final String CIPHER_PATH = "C:/Users/mrizz/Desktop/Workspaces/TESI/ciphertext.txt";
	
	
	public static void main(String[] args) 
	{
		File plain_file = new File(PLAIN_PATH);
		File cipher_file = new File(CIPHER_PATH);
		
		String plaintxt = Utilitas.leggiStringa(plain_file);
	    
		String ciphertxt = "";
		ciphertxt = cifraStringa(plaintxt, CIFRATO);
		Utilitas.scriviStringa(ciphertxt, cipher_file);
		
		
		System.out.println(plaintxt);
		System.out.println(ciphertxt);
		System.out.println("");
		System.out.println(Frequenze.calcolaFrequenze(ALFABETO, plaintxt));
		System.out.println(plaintxt.length() + " caratteri totali");
	}
	
	
	/**
	 * Metodo che cifra un carattere secondo un certo alfabeto cifrato
	 * @param c carattere da cifrare
	 * @param alfabetoCifrato stringa contenente l'alfabeto cifrato
	 * @return carattere cifrato
	 */
	public static char cifra(char c, String alfabetoCifrato)
	{
		return alfabetoCifrato.charAt(c-'a');
	}
	
	
	/**
	 * Metodo che cifra una stringa secondo un certo alfabeto cifrato
	 * @param plain stringa da cifrare
	 * @param alfabetoCifrato stringa contenente l'alfabeto cifrato
	 * @return stringa cifrata
	 */
	public static String cifraStringa(String plain, String alfabetoCifrato)
	{
		String cipher = "";
		
		int n = plain.length();
		
		for(int i = 0; i < n; i++)
		{
			// Cifro il testo lettera per lettera
			cipher = cipher + cifra(plain.charAt(i), alfabetoCifrato);
		}
		
		return cipher;
			
	}
}
